package hello;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;


@Configuration
class NoopSmtpCondition implements Condition {

    @Override
    public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
        boolean devEnvironment = "dev".equals(context.getEnvironment().getProperty("axel"));
        boolean forceEmail = "true".equals(context.getEnvironment().getProperty("force_email"));
        return devEnvironment || forceEmail;
    }
}
