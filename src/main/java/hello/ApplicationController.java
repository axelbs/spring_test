package hello;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.context.ApplicationContext;


@RestController
public class ApplicationController {

    @Value("${axel}")
    String property_from_kubernetes;

    @Autowired
    EmailSender emailSender;

    @Autowired
    private ApplicationContext appContext;

    @Bean
    @RequestMapping(value = "/testSend", method = RequestMethod.GET)
    public String apiController() {

        String retval = "Hello this is dog";
        String message_content = "this is the message content";

        try {
            /*
            if (property_from_kubernetes.equals("prod")) {
                sendSmtp("{PROD}: SENDS ALL THE EMAILS: prop=" + property_from_kubernetes);
            } else {
                System.out.println("{DEV}: LOG; SEND EMAIL: prop=" + property_from_kubernetes);
            }
            */
            emailSender.sendSmtp("some email message");
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.out.println(e.toString());
        }

        return retval + "\n";

    }

    public void sendSmtp(String message) {
        //imitate email sending
        System.out.println(message);

        return;
    }

}
