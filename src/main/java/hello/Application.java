package hello;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.Primary;

@SpringBootApplication
class Application {

    @Bean
    @Primary
    @ConditionalOnExpression("('${axel}'.equals('prod')) or ('${force}'.equals('true'))")
    public EmailSender prodEmailSender() {
        return new RealEmailSender();
    }

    @Bean
    @ConditionalOnExpression("(!'${axel}'.equals('prod'))")
    public EmailSender noopEmailSender() {
        return new NoopEmailSender();
    }

    public static void main(String[] args) {
        //System.setProperty("axel", "prod");
        SpringApplication.run(Application.class, args);
    }
}
